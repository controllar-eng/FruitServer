#!/usr/bin/env python3

# Common methods for new server
from extcommands import *
import serial
import socket
import re
import hashlib
import logging
import sqlite3
import configparser
# import datetime
# from subprocess import Popen, PIPE

# CONFIG
config = configparser.ConfigParser()
config_file = '/controllar/config.ini'
config.read(config_file)
default = 'DEFAULT'
NEW_SYSTEM = config.getboolean(default, 'new_system')
database_file = config.get(default, 'database_file')
webserver_id = config.get(default, 'webserver_id')
serial_device = config.get(default, 'serial_device')
serial_write_delay = config.getfloat(default, 'serial_write_delay')
serial_loop_delay = config.getfloat(default, 'serial_loop_delay')
server_interface = config.get(default, 'server_interface')
server_port = config.getint(default, 'server_port')
workers = config.getint(default, 'workers')
remote_access = config.getboolean(default, 'remote_access')
amazon_host = config.get(default, 'amazon_host')
amazon_port = config.getint(default, 'amazon_port')
heartbeat_wait = config.getint(default, 'heartbeat_wait')
amazon_retries_limit = config.getint(default, 'amazon_retries_limit')
retry_wait = config.getint(default, 'retry_wait')
mqtt_broker = config.get(default, 'mqtt_broker')
mqtt_broker_port = int(config.get(default, 'mqtt_broker_port'))
irconfig_file = config.get(default, 'irconfig_file')
broadlink_cmd_folder = config.get(default, 'broadlink_cmd_folder')
encodeir = config.get(default, 'encodeir')

# CONFIG

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)

try:
    with open('/controllar/config.json', "r") as json_data_file: 
        configuration = json.loads(json_data_file.read())
except FileNotFoundError:
    configuration = None

UTF_8 = 'UTF-8'

interface_address = (server_interface, server_port)
interface_address_command_ip = (server_interface, server_port + 1)
amazon_address = (amazon_host, amazon_port)

# This should not be hardcoded but it is only for logging purpose anyway
addressbook = {'54.94.228.34': 'AMAZON'}
client_name = 'CLIENT'

if NEW_SYSTEM:
    serial_command_pattern = b'\{00900[0-9][0-9]{4}\}'
    serial_ids = b'BID\?<([IP]n\\d+\\.\\d+)'
    serial_channels = b'ACH\?<(In\\d+\\.\\d+,\\d+,\\d+,\\d+)'
    serial_buttons = b'ABT\?<(In\\d+\\.\\d+,\\d+,\\d+,\\d+)'
    serial_info = b'FRV\?<(In\\d+\\.\\d+,\\d+,\\d+,\\d+,\\d+,\\d+)'
    serial_keypad_scene_protected = b'PTS\?<(Pn\\d+\\.\\d+,\\d+,\\d+)'
    serial_virtual_luminos = b'VRL\?<(In\\d+\\.\\d+,\\d+,\\d+)'
    serial_keypad_button = b'Pressiona botao Orig:\\s*(Pn\\d+\\.\\d+).*?Botao:(\\d+)'
    serial_keypad_lumino = b'\*\.\* Lumino:(\\d+)'
    serial_keypad_scene = b'TipoCena:(\\d+)'
    serial_keypad_snf = b'Sincronia Orig:'
    serial_keypad_is_scene = b'(STP\?<[IP]n\\d+\\.\\d+,\\d+,\\d+,\\d+)'
#    serial_command_pattern = b'\CFGCIT=>In*.*,[1-9],[0-9]{4}\}'
else:
    serial_command_pattern = b'I[0-9]{2}[0-9][0-9]{6}'

socket_command_pattern = b'I[0-9]{9}'

# old/new system intelligence numbering map
#   intel_out = 9 - math.frexp(intel_in)[1]
intel_out = {128: 1, 64: 2, 32: 3, 16: 4, 8: 5, 4: 6, 2: 7, 1: 8, 0: 0}
#   intel_in = pow(2, 8 - intel_out)
intel_in = {1: 128, 2: 64, 3: 32, 4: 16, 5: 8, 6: 4, 7: 2, 8: 1, 0: 0}


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


def get_database_hash(db_file_name):
    database = get_database(db_file_name)
    db_hash = hashlib.md5(database).hexdigest()
    return db_hash


def get_database(db_file_name):
    with open(db_file_name, 'rb') as f:
        read_data = f.read()
    return read_data


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


def receive_commands(listen_socket, buffered_data):
    received = listen_socket.recv(1024)
    if not received:
        raise EOFError('socket closed')
    buffered_data += received
    commands = buffered_data.split(b'\n')

    # it is ugly but for now it works :), will change anyway
    address = listen_socket.getpeername()[0]
    if address in addressbook:
        logging.debug('{:>10}: {}'.format(addressbook[address], buffered_data))
    else:
        logging.debug('{:>10}: {}'.format(client_name, buffered_data))

    return commands


def process_command_from_socket(command_string, raspberry_serial, lights):
    match = re.search(socket_command_pattern, command_string)
    matchBypass = re.search(b'^\$[A-Z]{3}.*$', command_string)
    logging.debug('{:>10}: {}'.format('raw', command_string))
    if match:
        command_string = match.group(0)
        kv = parse_command_string(command_string)
        compiled_string = compile_command_string_serial(kv[0], kv[1], lights)
        logging.debug('{:>10}: {}'.format('compiled', compiled_string))
        raspberry_serial.write(compiled_string)
        lumino = str(compiled_string).split(',')[1]
        intensity = str(kv[1]).split(',')[2][:-1]
        return int(lumino), int(intensity)
    elif matchBypass:
        logging.debug('{:>10}: {}'.format('bypass', command_string))
        raspberry_serial.write(command_string)
        raspberry_serial.write(b'\r\n')
        split_command_string = str(command_string).split(',')
        lumino = split_command_string[1]
        intensity = split_command_string[2][:-1]
        return int(lumino), int(intensity)
    else:
        logging.error('{:>10}: {}'.format('UNKNOWN', command_string))


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


def is_dimmer(value_tuple):
    return value_tuple[3] == 2

def is_ipcommand(inteligencia, canal):
    if not configuration:
        logging.info('something went wrong.')
        return False
    for externals in configuration:
        for command in externals['Commands']:
            if hasLumino(command, canal, inteligencia):
                return True
    return False

def compile_command_string_serial(key, value, ligths):
    if NEW_SYSTEM:
        return compile_command_string_serial_new(key, value, ligths)
    else:
        return compile_command_string_serial_old(key, value, ligths)


def compile_command_string_serial_old(key, value, ligths):
    string = 'I{:03d}{:02d}{}{}{:02d}'.format(key[0], key[1],
                                                value[0], value[1], value[2])
    return string.encode(UTF_8)


def compile_command_string_serial_new(key, value, ligths):
    onoff = value[0]
    intens = value[2]
    if is_dimmer(ligths[key]) and onoff == 0:
            intens = 0
  #  string = '{{010{:03d}{:02d}{:02d}}}'.format(intel_out[key[0]], key[1], intens)


###TODO: Check if there is not a problem with 'Dimmer' that is also IP Command!!!!

    elif is_ipcommand(key[0], key[1]):
#       Let's IP Command!      
    #### If sendMessage() returns a non empty list, auto-consider it a valid command
    #### So just short-circuit it directly to string-formatting
        if configuration:
            for externals in configuration:
                response = sendMessage(
                            buildMessage(key[0], key[1], intens, externals['Commands']), 
                            externals['Protocol'], 
                            externals['config'])

        else:
            intens = 0
    #string = '$CFGCIT=>In*.*,{:01d}{:02d},{:02d}\r\n'.format(intel_out[key[0]], key[1], intens)
    string = '$SET,{:01d}{:02d},{:02d}\r\n'.format(intel_out[key[0]], key[1], intens)
    return string.encode(UTF_8)


def compile_command_string_socket(key, value):
    string = 'I{:03d}{:02d}{}{}{:02d}\n\r'.format(key[0], key[1],
                                                value[0], value[1], value[2])
    return string.encode(UTF_8)


def parse_command_string(command_string):
    if chr(command_string[0]) == 'I':
        intel = int(command_string[1:4])
        canal = int(command_string[4:6])
        onoff = int(command_string[6:7])
        updown = int(command_string[7:8])
        intens = int(command_string[8:10])
        return (intel, canal), (onoff, updown, intens)
    elif chr(command_string[0]) == '{':
        logging.debug('{:>10}: {}'.format('raw', command_string))
        intel = intel_in[int(command_string[4:7])]
        intel_raw = int(command_string[4:7])
        canal = int(command_string[7:9])
        intens = int(command_string[9:11])
        return (intel, canal), intens, intel_raw


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


def connect_to_serial(serial_device):
    serial_port = serial.Serial(port=serial_device, baudrate=115200,
                                parity=serial.PARITY_NONE,
                                stopbits=serial.STOPBITS_ONE,
                                bytesize=serial.EIGHTBITS)
    logging.info('Connected socket to serial device {}'.format(serial_device))
    return serial_port


def connect_to_server(server_address):
    # server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # server_socket.connect(server_address)
    server_socket = socket.create_connection(server_address)
    logging.info('Connected socket to server {}'.format(server_socket.getpeername()[0]))
    return server_socket


def get_size_in_bytes(data):
    return len(data).to_bytes(4, byteorder='little')


def send_to_android_for_installation(sock, data_out):
    if isinstance(data_out, str):
        data_out = data_out.encode(UTF_8)
    size = get_size_in_bytes(data_out)
    sock.sendall(size)
    sock.sendall(data_out)


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


def load_lights_types(db_file_name):

    lights = {}

    conn = sqlite3.connect(db_file_name)
    c = conn.cursor()

    for rede in c.execute('SELECT inteligencia, canal, tipo FROM redes'):
        key = rede[0], rede[1]
        tipo = rede[2]
        intens = 35 if tipo == 2 else 0
        value = 0, 0, intens, tipo
        lights[key] = value

    conn.close()

    return lights


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


# KNOWN CLIENTS - experimental

# def known_clients():
#
#     ip_address = '192.168.11.111'
#     file_name = 'known_clients'
#
#     client_map = {}
#
#     with open(file_name, 'r') as file:
#         for line in file:
#             kv = line.split()
#             if len(kv) > 1:
#                 client_map[kv[0]] = kv[1]
#
#     pid = Popen(['arp', '-n', ip_address], stdout=PIPE)
#     s = pid.communicate()[0].decode(UTF_8)
#     mac_regex = '(([a-f\d]{1,2}\:){5}[a-f\d]{1,2})'
#     match = re.search(mac_regex, s)
#     if match:
#         mac = match.groups()[0]
#         if mac in client_map:
#             name = client_map[mac]
#             pat = '20[0-9]{2}-[0-9]{2}-[0-9]{2}'
#             if not re.match(pat, name):
#                 print(name)
#         else:
#             dt = '{:%Y-%m-%d %H:%M} '.format(datetime.datetime.now())
#             new_mac = '\n' + mac + ' ' + dt
#             with open(file_name, 'a') as file:
#                 file.write(new_mac)


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
