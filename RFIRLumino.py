from abc import ABC, abstractmethod
import math
import os
import subprocess
import yaml
import time
import logging
import hashlib


class RFIRLumino(ABC):

    def __init__(self, mqtt_client, broadlink_cmd_folder,
                 encodeir="encodeir", channel=0, freq=38000,
                 logging=None):
        self.mqtt_client = mqtt_client
        self.channel = channel
        self.freq = freq
        self.broadlink_cmd_folder = broadlink_cmd_folder
        self.mqtt_msgs = None
        self.default_msg = None
        self.encodeir = encodeir
        self.logging = logging

    def command_to_mqtt(self, cmds_cfgs, default_cmd_cfg):
        if default_cmd_cfg:
            try:
                self.default_msg = self.get_mqtt_msg_from_command(
                    default_cmd_cfg)
            except Exception as e:
                if self.logging:
                    self.logging.error('{}'.format(str(e)))
        if cmds_cfgs:
            self.mqtt_msgs = {}
            for key in cmds_cfgs:
                try:
                    self.mqtt_msgs[key] = self.get_mqtt_msg_from_command(
                        cmds_cfgs[key])
                except Exception as e:
                    if self.logging:
                        self.logging.error('{}'.format(str(e)))

    @staticmethod
    def _is_sendir_command(command):
        if isinstance(command, str) and command.startswith("sendir"):
            return True
        return False

    def _is_broadlink_file(self, command):
        if self._is_sendir_command(command):
            return False
        return isinstance(command, str)

    @staticmethod
    def _is_protocol(command):
        if type(command) is not dict:
            return False
        if ("protocol" not in command or "device" not in command or "subdevice"
           not in command or "function" not in command):
            return False
        return True

    @abstractmethod
    def get_mqtt_msg_from_command(self, command):
        pass  # pragma: no cover

    def _ir_record_signal(self):
        pass  # pragma: no cover

    def _rf_record_signal(self):
        pass  # pragma: no cover

    @staticmethod
    def _pulesarray_to_broadlink(pulesArray, freq):
        broadlink_header = ''
        broadlink_body = ''

        # IR frequency
        broadlink_header += "{:02x}".format(round(freq/1000))
        # Add don't repeat byte flag
        broadlink_header += '00'

        for i in pulesArray:
            puls = round(i * 269 / 8192)
            hexCommand = "{:02x}".format(puls)
            if len(hexCommand) > 2:
                # if it can't stored in one byte, pad it to double byte
                # and add the '00' flag (this flag mark that the next value is
                # two bytes and not only one)
                hexCommand = "00" + "{:04x}".format(puls)

            # Add the command to the final broadlink command
            broadlink_body += hexCommand

        # Add length to the header (with adicional 3 for OFF constant of the
        # footer)
        length = "{:04x}".format(int(len(broadlink_body) / 2) + 3)
        # change to little endien
        broadlink_header += length[2:] + length[:2]
        # Add the IR command ends flag (for IR only)
        broadlink_footer = "000d05000000000000000000000000"

        return broadlink_header + broadlink_body + broadlink_footer

    def _sendir_to_pulesarray(self, command):
        sendir_command = command.split(":")[1]
        pulesarray = [int(x) for x in sendir_command.split(',')]
        self.freq = pulesarray[2]
        pulesarray = pulesarray[5:]

        for i in range(len(pulesarray)):
            pulesarray[i] = int((pulesarray[i]/self.freq) * 1000000)

        return pulesarray

    @staticmethod
    def _generate_hash_for_sendir(command):
        command = command.encode('utf-8')
        return "sendir-" + hashlib.sha256(command).hexdigest()

    def activate(self, intensity):
        if self.mqtt_msgs and intensity in self.mqtt_msgs:
            mqtt_msg = self.mqtt_msgs[intensity]
        else:
            if not self.default_msg:
                if self.logging:
                    self.logging.error('Could not activate lumino')
                return
            mqtt_msg = self.default_msg
        self.mqtt_client.publish(mqtt_msg['topic'], mqtt_msg['msg'])
        if self.logging:
            self.logging.debug('{:>10}: {} | {}'.format(
                'MQTT', mqtt_msg['topic'],  mqtt_msg['msg']))


class RFIRLuminoThmedia(RFIRLumino):

    def __init__(self, mqtt_client, broadlink_cmd_folder,
                 encodeir="encodeir", mac="ff_ff_ff_ff_ff_ff",
                 channel=0, freq=38000, signal="ir", logging=None):

        super().__init__(mqtt_client, broadlink_cmd_folder,
                         encodeir, channel, freq, logging)

        self.signal = signal
        self.command_file = None
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message

        self.record_time = 10
        self.msg_recorded = None

        self.mac = mac.replace("_", ":").upper()
        self.ir_topic = "ir_server/" + self.mac + "/received"
        self.rf_topic = "ir_server/" + self.mac + "/received_rf"

    # based on
    # https://github.com/haimkastner/broadlink-ir-converter/blob/master/src/index.ts
    @staticmethod
    def _broadlink_to_pulesarray(broadlinkHexCommand):

        def readNextByte(hexCommand):
            return hexCommand[0:2], hexCommand[2:]

        pulesArray = []
        isIr = False

        currentByte, hexCommand = readNextByte(broadlinkHexCommand)
        if currentByte == '26':
            isIr = True
        # else: Ignore the RF frequency

        # Read the 'repeat byte' (and ignore it)
        currentByte, hexCommand = readNextByte(hexCommand)

        # Read the length bytes (ignore them too)
        currentByte, hexCommand = readNextByte(hexCommand)
        currentByte, hexCommand = readNextByte(hexCommand)

        while True:
            currentByte, hexCommand = readNextByte(hexCommand)

            # If its '00' it means the next hex pules value was larger then one
            # byte. So the broadlink mark by '00' byte that the next byte
            # belong his following byte.
            # For example the 0x123 value will be in shown as '00' '01' '23'
            # so we need to skip the '00' flag and combine the next tow bytes
            # to one hex value
            if currentByte == '00':
                # Read the first byte
                currentByte, hexCommand = readNextByte(hexCommand)
                higherByte = currentByte
                # Read the second byte
                currentByte, hexCommand = readNextByte(hexCommand)
                lowerByte = currentByte
                # Combine the two bytes (big-endian)
                currentByte = higherByte + lowerByte

            # Convert the hex value to a decimal number
            pulsFactor = int(currentByte, 16)
            # Get the pulse lengths (revers the formula: µs * 2^-15))
            puls = pulsFactor / 269 * 8192
            # Flat the pules number
            flatPuls = math.floor(puls)

            pulesArray.append(flatPuls)

            # If the all command converted, or in IR the end flag shown, break
            # the loop
            if not hexCommand or (
               isIr and hexCommand.lower().startswith('000d05')):
                break

        return pulesArray

    def _broadlink_file_to_raw(self, cmd_file):
        with open(os.path.join(self.broadlink_cmd_folder, cmd_file)) as f:
            broadlinkHexCommand = f.read()
            pulesArray = self._broadlink_to_pulesarray(broadlinkHexCommand)

        # Prepend thmedia code "30" for raw data, and frequency
        pulesArray.insert(0, 30)
        pulesArray.insert(1, self.freq)

        return ','.join(map(str, pulesArray))

    def _protocol_to_raw(self, protocol, device, subdevice, function):
        pules = subprocess.check_output([self.encodeir, str(protocol),
                                         str(device), str(subdevice),
                                         str(function)])
        header = "30," + str(self.freq) + ","
        body = pules.decode('utf-8').replace(" ", ",")
        return header + body

    def _sendir_to_raw(self, command):
        pulesarray = self._sendir_to_pulesarray(command)
        pulesarray = [30, self.freq] + pulesarray
        return ','.join(str(n) for n in pulesarray)

    def _get_raw_command(self, command):
        # always convert files to raw, in case the file got updated
        self.command_file = command
        try:
            if self._is_broadlink_file(command):
                return self._broadlink_file_to_raw(command)

            if self._is_sendir_command(command):
                return self._sendir_to_raw(command)

            if not self._is_protocol(command):
                raise Exception("command or signal type not compatible")

            return self._protocol_to_raw(command["protocol"],
                                         command["device"],
                                         command["subdevice"],
                                         command["function"])
        except Exception:
            realpath = os.path.join(self.broadlink_cmd_folder,
                                    self.command_file)
            if os.path.exists(realpath):
                with open(realpath) as f:
                    msg = f.read()
                return msg
            return ""

    def get_mqtt_msg_from_command(self, command):
        msg = self._get_raw_command(command)
        if not msg:
            if (self.signal == "ir" and self._ir_record_signal()
               and self.logging):
                self.logging.info('{}'.format('ir command recorded!'))
            elif (self.signal == "rf" and self._rf_record_signal()
                  and self.logging):
                self.logging.info('{}'.format('rf command recorded!'))
            else:
                self.logging.info('{}'.format('record failed!'))
        topic = "ir_server/" + self.mac + "/send_" + str(self.channel)
        return {"msg": msg, "topic": topic}

    def _ir_record_signal(self):
        if self.logging:
            self.logging.info('{:>10}'.format('recording ir signal...'))
        self.mqtt_client.subscribe(self.ir_topic)
        while self.record_time:
            if self.msg_recorded:
                return True
            time.sleep(1)
            self.record_time = self.record_time - 1
        return False

    def _rf_record_signal(self):
        if self.logging:
            self.logging.info('{:>10}'.format('recording rf signal...'))
        self.mqtt_client.subscribe(self.rf_topic)
        while self.record_time:
            if self.msg_recorded:
                return True
            time.sleep(1)
            self.record_time = self.record_time - 1
        return False

    def on_message(self, client, userdata, message):
        self.msg_recorded = message.payload.decode("UTF-8")

        if self.logging:
            self.logging.debug('{:>10}: {}'.format(
                'MSG (raw)', self.msg_recorded))
        if self.signal == "ir":
            msg_recorded_raw = self.msg_recorded[:-11]
            self.mqtt_client.unsubscribe(self.ir_topic)
        else:
            msg_recorded_raw = self.msg_recorded + self.msg_recorded[:-1]
            self.mqtt_client.unsubscribe(self.rf_topic)
        realpath = os.path.join(self.broadlink_cmd_folder,
                                self.command_file)
        os.makedirs(os.path.dirname(realpath), exist_ok=True)
        with open(realpath, 'w') as f:
            f.write("30," + str(self.freq) + "," + msg_recorded_raw)

    def on_connect(self, client, data, flags, rc):
        logging.info('{}'.format("Connected to receive ir signal"))


class RFIRLuminoBroadlink(RFIRLumino):

    def __init__(self, mqtt_client, broadlink_cmd_folder,
                 encodeir="encodeir", mac="ff_ff_ff_ff_ff_ff",
                 channel=0, freq=38000, signal="ir", logging=None):

        self.mac = mac
        self.command_file = None
        self.signal = signal

        super().__init__(mqtt_client, broadlink_cmd_folder,
                         encodeir, channel, freq, logging)

    def _protocol_to_broadlink(self, protocol, device, subdevice, function):
        pules = subprocess.check_output([self.encodeir, str(protocol),
                                        str(device), str(subdevice),
                                        str(function)])
        pulesarray = [int(x) for x in pules.split()]
        return self._pulesarray_to_broadlink(pulesarray, self.freq)

    def _sendir_to_broadlink(self, command):
        pulesarray = self._sendir_to_pulesarray(command)
        return self._pulesarray_to_broadlink(pulesarray, self.freq)

    def _get_broadlink_command(self, command):
        if self._is_broadlink_file(command):
            return os.path.join("broadlink/", self.mac, command)

        elif self._is_sendir_command(command):
            self.command_file = self._generate_hash_for_sendir(command)
            realpath = os.path.join(self.broadlink_cmd_folder,
                                    self.command_file)
            if os.path.exists(realpath):
                return os.path.join("broadlink/", self.mac, self.command_file)
            broadlink_hex = self._sendir_to_broadlink(command)

        elif self._is_protocol(command):
            if self.signal != "ir":
                raise Exception("signal type not compatible")

            self.command_file = os.path.join(str(command["protocol"]),
                                             str(command["device"]),
                                             str(command["subdevice"]),
                                             str(command["function"]))
            realpath = os.path.join(self.broadlink_cmd_folder,
                                    self.command_file)
            if os.path.exists(realpath):
                return os.path.join("broadlink/", self.mac, self.command_file)
            broadlink_hex = self._protocol_to_broadlink(command["protocol"],
                                                        command["device"],
                                                        command["subdevice"],
                                                        command["function"])

        else:
            raise Exception("command type unknown")

        os.makedirs(os.path.dirname(realpath), exist_ok=True)
        with open(realpath, 'w') as f:
            f.write(broadlink_hex)

        return os.path.join("broadlink/", self.mac, self.command_file)

    def get_mqtt_msg_from_command(self, command):
        if self.signal == "ir":
            msg = "auto"
        else:
            msg = "autorf"
        topic = self._get_broadlink_command(command)
        return {"msg": msg, "topic": topic}


class RFIRLuminoList:

    def __init__(self, mqtt_client, broadlink_cmd_folder, irconfig_file,
                 encodeir="encodeir", logging=None):
        self.mqtt_client = mqtt_client
        self.broadlink_cmd_folder = broadlink_cmd_folder
        self.encodeir = encodeir
        self.logging = logging
        self.irconfig_file = irconfig_file

        self.luminos = {}
        if os.path.exists(irconfig_file):
            self.yaml_modified_time = time.ctime(os.path.getctime(
                                                 irconfig_file))
        else:
            self.yaml_modified_time = 0
            return

        self.config = self.from_yaml_file(irconfig_file)
        self.create_lumino_list()

    def from_yaml_file(self, yaml_file):
        with open(yaml_file) as f:
            try:
                return yaml.load(f, Loader=yaml.FullLoader)
            except Exception as e:
                if self.logging:
                    self.logging.debug('{}'.format(e))

    def create_lumino_list(self):
        mac = "ff_ff_ff_ff_ff_ff"
        if not self.config:
            return
        for k, i in self.config.items():
            if i["emitter"].lower() == "broadlink":
                self.luminos[k] = RFIRLuminoBroadlink(self.mqtt_client,
                                                      self.broadlink_cmd_folder, # noqa
                                                      self.encodeir,
                                                      i.get("mac", mac),
                                                      i.get("channel", 1),
                                                      i.get("freq", 38000),
                                                      i.get("signal", "ir"),
                                                      self.logging)
            elif i["emitter"].lower() == "thmedia":
                self.luminos[k] = RFIRLuminoThmedia(self.mqtt_client,
                                                    self.broadlink_cmd_folder,
                                                    self.encodeir,
                                                    i.get("mac", mac),
                                                    i.get("channel", 0),
                                                    i.get("freq", 38000),
                                                    i.get("signal", "ir"),
                                                    self.logging)
            else:
                raise Exception("Invalid emitter")

            self.luminos[k].command_to_mqtt(
                i.get("commands"), i.get("command"))

    def is_yaml_file_different(self):
        if os.path.exists(self.irconfig_file) and (time.ctime(os.path.getmtime(
                                                   self.irconfig_file))
           != self.yaml_modified_time) and self.irconfig_file:
            self.yaml_modified_time = time.ctime(os.path.getmtime(
                                                 self.irconfig_file))
            return True
        else:
            return False

    def activate(self, lumino, intensity):
        if self.is_yaml_file_different():
            self.config = self.from_yaml_file(self.irconfig_file)
            self.luminos = {}
            self.create_lumino_list()
        lum = self.luminos.get(lumino)
        if lum:
            lum.activate(intensity)
