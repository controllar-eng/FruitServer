import json
import re
import itertools
import socket
import requests
from time import sleep
from functools import reduce
from telnetlib import Telnet

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
####Dummy Methods

def dummyResponse(message):
    if not message:
        return ''
    if "SI" in message:
        dummyResp = "SIBD"
    elif "PW" in message:
        dummyResp = "PWON"
    elif "MV?" in message:
        dummyResp = "MV155"
    elif "MVUP" in message:
        dummyResp = "MV205"
    elif "MVDOWN" in message:
        dummyResp = "MV105"
    elif "MU" in message:
        dummyResp = "MUOFF"
    elif "MV" in message:
        dummyResp = message
    else:
        dummyResp = "IGNORE"
    return dummyResp
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
####Protocol Communication stuff


def ipCommandComm(message, config):

    #'''    
    #working, don't touch
    #print("Let's start this!!")
    sleep(0.1)
    try:
        tn = Telnet(config['ipaddress'], config['port'], 3)

        #print("Connected to Something")
        sleep(0.1)

        tn.write(message.encode('ascii')+b"\r")

        #print("Wrote "+ message)

        response = tn.read_until(b"\r", 0.5).decode().replace('\r', '')

        #print("Closing conection")
        tn.close()

        #if response: 
         #   print(response)
        return response

    except:
        #print("No available server.")
        return 'DISABLED'
    '''
        
    
    print(config['ipaddress']+":", config['port'])
    if message: 
        print(message+"<CR>")
        sleep(0.1)
    response = dummyResponse(message)
    print("Response: "+response)
    return response
    #'''

def hasLumino(command, canal, inteligencia):
    return command['inteligencia'] == inteligencia and command['canal'] == canal

def getLumino(command):
    return dict({'inteligencia' : command['inteligencia'], 'canal' : command['canal'], 'intensidade': command['intensidade']})
        
def statusMessage(message):
    return message['status']


#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
####Protocol Communication stuff

def httpCommandComm(message, config):

    if not message:
        return
 
    try:
        r = requests.get(config['URI']+message, timeout=1.0)
    except:
        return ""
    return ""
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
####Protocol Communication stuff

def tcpCommandComm(message, config):

    return ""
'''
    def socket_init_connection(message):
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host = "aplicacao.controllar.com"
        port = 8900
        #print("Sending this: "+ message)
    #    message = "XLARANJA\nI128251150\n\rI128250000\n\r"
        try:
            client.connect((host, port))

        except:
            return "Failed to Connect to Host"

        try:
            client.send(message);
        except:
            #print("Failed to Connect")
            return "FAILED TO CONNECT."
        
        try:
            client.close
        except:
            #print("Failed to close")
            return "Failed to Disconnect."

        return ""

    #print("Message is: ", message)
    socket_init_connection(message)
    return
    
    print("Connecting via TCP")
    sleep(0.1)

    try:
        #print("Trying to connect to:")
        #print(config['ipaddress'], config['port'])
        TCP_IP = config['ipaddress']
        TCP_PORT = config['port']

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))
        s.send(message.encode('UTF-8')+b"\r")
        data = s.recv(BUFFER_SIZE)
        #print("IT worked and data is: ")
        #print(data)
        s.close()

                
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((config['ipaddress'], config['port']))

        sleep(0.1)

        s.sendall(message.encode('ascii')+b"\r")

        #print("Wrote "+ message)
        #print("Closing conection")

        s.close()
    

    except:
        #print("No available server.")
        return 'DISABLED'    

    #if response: 
        #print(response)
    return response


    
    if not message:
        return

    print(config['ipaddress']+":", config['port'])
    if message: 
        print(message+"<CR>")
    response = dummyResponse(message)
    print("Response: "+response)
    return response

    return
'''

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

def sendMessage(messages, protocol, config):

    if not messages:
        return

    communication = {
        'ipcommand': ipCommandComm,
        'http': httpCommandComm,
        'tcp': tcpCommandComm
    }
    return list(map(lambda message: communication[protocol](message, config), messages))

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#
def buildMessage(inteligencia, canal, intensidade, commands):
    def onoffMessageBehaviour(message, intensidade):
        if intensidade == 50:
            return message['message']['on']
        return message['message']['off']
    
    def switchMessageBehaviour(message, intensidade):
        if intensidade == 50:
            return message['message']
        return ''
    
    def monitorMessageBehaviour(message, intensidade):
        return message['message']
    
    def pulseMessageBehaviour(message, intensidade):
        if intensidade == 50:
            return message['message']
        return ''
    
    def dimmerMessageBehaviour(message, intensidade):
        #print("Parser is: ", message['parser'])
        sendMessage = []
        for parser in message['parser']:
            sendMessage.append(parser+str(intensidade))
            #print("Partial message is: ",sendMessage)
            return list(sendMessage)
        #return message['parser'] + str(2*intensidade)
        #print("Message is: ", sendMessage)
        return list(sendMessage)
    behaviour = {
        'OnOff': onoffMessageBehaviour,
        'Switch': switchMessageBehaviour,
        'Pulse': pulseMessageBehaviour,
        'Monitor': monitorMessageBehaviour,
        'Dimmer': dimmerMessageBehaviour
    }

    return  list(itertools.chain.from_iterable(map(lambda command: behaviour[command['type']](command, intensidade) , 
                filter(lambda command: 'canal' and 'inteligencia' and 'type' in command and hasLumino(command, canal, inteligencia) , 
                    commands))))


def buildStatusMessage(commands):
    return (list(set(map((lambda command: command['status']), filter(lambda command: 'status' in command, commands)))))

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
def buildFromResponse(commands, responses):
    def onoffResponseBehaviour(message, command):
        if 'message' in command and message in command['message']['on'] and "ON" in message:
            command['intensidade'] = 50
            return command['message']['on']
            
        elif 'message' in command and message in command['message']['off'] and ("OFF" in message or "STANDBY" in message):
            command['intensidade'] = 0
            return command['message']['off']

        elif 'DISABLED' in message:
            command['intensidade'] = 0
            return command['message']['off']

    def switchResponseBehaviour(message, command):
        if 'message' in command and message in command['message']:
            for commands in command['message']:
                if message in commands:
                    command['intensidade'] = 50
                    return command['message']

        elif 'DISABLED' in message:
            command['intensidade'] = 0
            return command['message']

    def monitorResponseBehaviour(message, command):
        
        if 'parser' in command:
            for parser in command['parser']:
                if parser in message:
                    command['intensidade'] = int(re.sub(parser,"", message))
                    print("Intensity at = "+str(command['intensidade']))
                    return message
        
        elif 'DISABLED' in message:
            command['intensidade'] = 0
            return command['message'] + str(command['intensidade'])
        
        return ''
    
    def pulseResponseBehaviour(message, command):   

        if 'parser' in command:
            for parser in command['parser']:
                if parser in message:
                    command['intensidade'] = 0
                    return command['message']
        
        elif 'DISABLED' in message:
            command['intensidade'] = 0
            return command['message']

        command['intensidade'] = 0        
        return ''
    
    def dimmerResponseBehaviour(message, command):
        if 'parser' in command:
            for parser in reversed(command['parser']):
                if parser in message:
                    intensidade = re.sub(parser, "", message)
                    #print("Intensidade = "+intensidade)
                    if len(intensidade) == 3:
                        intensidade = intensidade[0:2]
                    elif intensidade[0] == "0":
                        intensidade = re.sub("0", "", intensidade)

                    command['intensidade'] = int(int(intensidade))

                    #print("Intensity at = "+str(command['intensidade']))
                    return message
            
        elif 'DISABLED' in message:
            command['intensidade'] = 0
            return command['parser'][0] + str(command['intensidade'])
        
        return ''       
        
#        if 'message' in command and message in command['message']:
            #do logic to parse string to get Integer
#            return command['message']

    def getCommand(commands, message):
        return list(map(lambda command: getLumino(command), filter(lambda command: 'canal' and 'inteligencia' and 'message' and message and behaviour[command['type']](message, command), commands)))

    behaviour = {
        'OnOff': onoffResponseBehaviour,
        'Switch': switchResponseBehaviour,
        'Pulse': pulseResponseBehaviour,
        'Monitor': monitorResponseBehaviour,
        'Dimmer': dimmerResponseBehaviour
    }

    print("Building object from Response")
    if not responses:
        return [] 
    return list(
                reduce(lambda x,y: x+y, 
                        list(
                            map(lambda response : getCommand(commands, response), responses))))

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-




